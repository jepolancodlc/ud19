
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MiniEncuestaView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	
	public MiniEncuestaView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 449, 355);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Elije un sistema operativo (solo una opcion).");
		lblNewLabel.setBounds(12, 13, 294, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblElijaSuEspecialidad = new JLabel("Elija su especialidad (pueden seleccionar ninguna o varias opciones).");
		lblElijaSuEspecialidad.setBounds(12, 73, 454, 16);
		contentPane.add(lblElijaSuEspecialidad);
		
		JLabel lblHorasDedicadasAl = new JLabel("Horas dedicadas al ordenador");
		lblHorasDedicadasAl.setBounds(12, 192, 454, 16);
		contentPane.add(lblHorasDedicadasAl);
		
		JRadioButton rbotonWin = new JRadioButton("Windows");
		rbotonWin.setActionCommand("Windows");;
		buttonGroup.add(rbotonWin);
		rbotonWin.setBounds(12, 39, 81, 25);
		contentPane.add(rbotonWin);
		
		JRadioButton rbotonlinux = new JRadioButton("Linux");
		rbotonlinux.setActionCommand("Linux");
		buttonGroup.add(rbotonlinux);
		rbotonlinux.setBounds(97, 38, 81, 25);
		contentPane.add(rbotonlinux);
		
		JRadioButton rbotonMac = new JRadioButton("MAC");
		rbotonMac.setActionCommand("MAC");
		buttonGroup.add(rbotonMac);
		rbotonMac.setBounds(179, 39, 127, 25);
		contentPane.add(rbotonMac);
		
		JCheckBox checkProgramacion_1 = new JCheckBox("Programacion");
		checkProgramacion_1.setBounds(12, 98, 113, 25);
		contentPane.add(checkProgramacion_1);
		
		JCheckBox checkDiseno = new JCheckBox("Dise\u00F1o Grafico");
		
		checkDiseno.setBounds(12, 128, 113, 25);
		contentPane.add(checkDiseno);
		
		JCheckBox checkAdmin = new JCheckBox("Administraci\u00F3n");
		
		
		checkAdmin.setBounds(12, 158, 113, 25);
		contentPane.add(checkAdmin);
		
		JSlider slider = new JSlider(0, 10);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setMaximum(10);
		slider.setValue(0);
		slider.setBounds(12, 216, 200, 36);
		contentPane.add(slider);

		
		JButton btnNewButton = new JButton("Listo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				ArrayList<String> checkSelect = new ArrayList<String>();
				if(checkProgramacion_1.isSelected())
					checkSelect.add(checkProgramacion_1.getText());
				if(checkDiseno.isSelected())
					checkSelect.add(checkDiseno.getText());
				if(checkAdmin.isSelected())
					checkSelect.add(checkAdmin.getText());
				
				JOptionPane.showMessageDialog(null, "Su sistema operativo es " + buttonGroup.getSelection().getActionCommand()  +
						"\nEs especialista en  "  + checkSelect + 
						"\nHoras dedicadas al ordenador: " + slider.getValue()+ "h");
				} 
		});
		btnNewButton.setBounds(12, 252, 97, 25);
		contentPane.add(btnNewButton);
	}
	
}
