import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;

public class MiniCalculadora extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfOp1;
	private JTextField tfOp2;
	private JTextField tfResultado;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnSuma;
	int resultado;
	
	public MiniCalculadora() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 477, 256);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfOp1 = new JTextField();
		tfOp1.setBounds(12, 74, 116, 22);
		contentPane.add(tfOp1);
		tfOp1.setColumns(10);
		
		tfOp2 = new JTextField();
		tfOp2.setBounds(154, 74, 116, 22);
		contentPane.add(tfOp2);
		tfOp2.setColumns(10);
		
		tfResultado = new JTextField();
		tfResultado.setEditable(false);
		tfResultado.setFont(new Font("Tahoma", Font.PLAIN, 34));
		
		tfResultado.setBounds(287, 37, 157, 59);
		contentPane.add(tfResultado);
		tfResultado.setColumns(10);
		
		btnSuma = new JButton("Suma");
		btnSuma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
		int op1=Integer.parseInt(tfOp1.getText()); 	
		int op2=Integer.parseInt(tfOp2.getText()); 
		resultado= op1 + op2; 
		tfResultado.setText(Integer.toString(resultado));

			}
		});
		buttonGroup.add(btnSuma);
		btnSuma.setBounds(12, 110, 97, 25);
		contentPane.add(btnSuma); 
		
		JButton btnResta = new JButton("Resta");
		btnResta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int op1=Integer.parseInt(tfOp1.getText()); 	
				int op2=Integer.parseInt(tfOp2.getText()); 
				resultado= op1 - op2;
				tfResultado.setText(Integer.toString(resultado));

			}
		});
		buttonGroup.add(btnResta);
		btnResta.setBounds(121, 110, 97, 25);
		contentPane.add(btnResta);
	
		JButton btnMulti = new JButton("Multiplicar");
		btnMulti.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int op1=Integer.parseInt(tfOp1.getText()); 	
				int op2=Integer.parseInt(tfOp2.getText()); 
				resultado= op1 * op2;
				tfResultado.setText(Integer.toString(resultado));

			}
		});
		buttonGroup.add(btnMulti);
		btnMulti.setBounds(238, 109, 97, 25);
		contentPane.add(btnMulti);
		
		JButton btnDividir = new JButton("Dividir");
		btnDividir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int op1=Integer.parseInt(tfOp1.getText()); 	
				int op2=Integer.parseInt(tfOp2.getText()); 
				resultado= op1 / op2;
				tfResultado.setText(Integer.toString(resultado));

			}
		});
		buttonGroup.add(btnDividir);
		btnDividir.setBounds(347, 109, 97, 25);
		contentPane.add(btnDividir);
		
		JButton btnAbout = new JButton("About");
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "Programa creado por John Polanco para Fundación Esplai.", "ABOUT APP", JOptionPane.INFORMATION_MESSAGE);
		}});
		btnAbout.setBounds(347, 171, 97, 25);
		contentPane.add(btnAbout);
		
		JLabel labelOp1 = new JLabel("Operador 1");
		labelOp1.setBounds(12, 56, 87, 16);
		contentPane.add(labelOp1);

		JLabel labelOp2 = new JLabel("Operador 2");
		labelOp2.setBounds(154, 56, 87, 16);
		contentPane.add(labelOp2);
		
		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setBounds(287, 13, 87, 16);
		contentPane.add(lblResultado);
	}
}
