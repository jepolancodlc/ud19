import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;



public class SaludadorView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfNombre;

	public SaludadorView() {
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfNombre = new JTextField();
		tfNombre.setBounds( 45,100, 354, 25);
		contentPane.add(tfNombre);
		
		JButton btSaludar = new JButton("\u00A1Saludar!");
		btSaludar.setBounds(169, 155, 98, 25);
		btSaludar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btSaludar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {JOptionPane.showMessageDialog(null, "�Hola " + tfNombre.getText() + "!");
			}
		});
		contentPane.add(btSaludar);
		
		JLabel lblNewLabel = new JLabel("Escriba un nombre para saludar");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(112, 57, 246, 16);
		contentPane.add(lblNewLabel);
	}
}
